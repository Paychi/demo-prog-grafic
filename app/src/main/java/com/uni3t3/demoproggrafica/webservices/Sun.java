package com.uni3t3.demoproggrafica.webservices;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alvinbaltodano on 6/13/17.
 */

public class Sun {

    Context context;

    public Sun(Context ctx){
        Log.i("Click<>", "class");
        context = ctx;

        String url = "http://samples.openweathermap.org/data/2.5/weather?lat=35&lon=139&appid=b1b15e88fa797225412429c1c50c122a1";

        JsonObjectRequest jsonObject = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Click<>", response.toString());

                if(response != null){

                    Log.i("Click<>", response.toString());
                    try {
                        Toast.makeText(context, response.getString("name"), Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.i("Click<>", response.toString());
                    }


                }else{
                    Toast.makeText(context, "No data.", Toast.LENGTH_SHORT).show();
                    Log.i("Click<>", response.toString());
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("GetDesc<>", "Error: "+ error.toString());
            }
        });

        jsonObject.setRetryPolicy(new DefaultRetryPolicy(60 * 1000, 1, 1.0f));
        ConnectionVolley.getInstance(context).getRequestQueue().add(jsonObject);
    }
}
