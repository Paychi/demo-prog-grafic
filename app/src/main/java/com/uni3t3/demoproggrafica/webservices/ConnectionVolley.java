package com.uni3t3.demoproggrafica.webservices;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by alvinbaltodano on 3/10/17.
 */

public class ConnectionVolley {

    private static ConnectionVolley myInstance;
    private RequestQueue request;
    private static Context context;

    public ConnectionVolley(Context context) {
        this.context = context;
        this.request = Volley.newRequestQueue(context);
    }

    public static synchronized ConnectionVolley getInstance(Context context){
        if (myInstance == null){
            myInstance = new ConnectionVolley(context);
        }

        return myInstance;
    }

    public RequestQueue getRequestQueue() {
        if (request == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            request = Volley.newRequestQueue(context.getApplicationContext());
        }
        return request;
    }
}
